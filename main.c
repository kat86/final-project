#include "utils.h"
#include "MK64F12.h"
#include "3140_concur.h"
#include <stdlib.h>

realtime_t* start= NULL;
realtime_t	limit;
realtime_t* current_time= NULL;/* Current elapsed time */

void enable_buttons(void) {
	SIM->SCGC5 |= SIM_SCGC5_PORTC_MASK; //Enable the clock to port C
	SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK; //Enable the clock to port A
	SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK;
	PORTC->PCR[6] = (1 << 8); //sw2
	PORTA->PCR[4] = (1 << 8); //sw3
	PORTA->PCR[1] = (1 << 8); //sw2 on cr20a
	PORTB->PCR[23] = (1 << 8); //sw1 on cr20a
}

//helper function to get time when user presses button to start game
realtime_t user_start(void) {
	int i;
	while (PTC->PDIR & (1<<6));
	start= current_time;
	cr20a_green_off(); //turns off green LED once user is sure the shield is properly aligned to board
	for (i=0; i<3; i++) {
		delay();
	}
	return *current_time;
}

//random helper function
int random(void) {
	int r;
	r= rand() % 24; //get random int mod 24 for which button/combination of buttons to display
	if (r==0 | r==5 | r==23) {
		r= 0;
	}
	else if (r==1 | r ==14 | r ==11) {
		r= 1;
	}
	else if (r==2 | r==13 | r==16) {
		r = 2;
	}
	else if (r==3 | r==15 | r==7) {
		r= 3;
	}
	else if (r==6 | r== 9 | r == 20){
		r= 4;
	}
	else if (r==4 | r == 19 | r == 21) {
		r= 5;
	}
	else if (r==8 | r == 17 | r == 18) {
		r= 6;
	}
	else {
		r= 7;
	}
	return r;
}

void seed(realtime_t time) { //seeds the random number generator used to figure out which combination of LEDs will light
	unsigned int t;
	t= time.sec + 0.001*time.msec;
	srand(t);
}

realtime_t* add_time(realtime_t* t1, realtime_t* t2) { //adding two realtime_t values
	int msec= t1->msec + t2->msec;
	int sec= t1->sec + t2->sec;
	realtime_t* r;
	if (msec >= 1000) {
		msec= msec % 1000;
		sec++;
	}
	r->sec = sec;
	r->msec= msec;
	return r;
}

int less_than(realtime_t* t1, realtime_t* t2) { //checks if realtime_t* t1 is less than t2. returns 1 if true, 0 if false.
	if (t1->sec > t2->sec) {
		return 0;
	}
	else if (t1->sec < t2->sec) {
		return 1;
	}
	else if (t1->msec < t2->msec) {
		return 1;
	}
	else {
		return 0;
	}
}

void timer_change(void) { //changes limit based on how long game has been played (as game goes on, limit becomes shorter, making game harder
	if (current_time->sec < 20) {
		limit.sec= 2;
		limit.msec=0;
	}
	else if (current_time->sec < 40) {
		limit.sec= 1;
		limit.msec= 0;
	}
	else if (current_time->sec < 60) {
		limit.sec= 0;
		limit.msec= 750;
	}
	else {
		limit.sec = 0;
		limit.msec= 500;
	}
}

void game(void) { //game function
	realtime_t roundstart; //start of round
	realtime_t* t3;
	int r;
	roundstart.sec= current_time->sec;
	roundstart.msec= current_time->msec;
	timer_change();
	t3= add_time(&roundstart, &limit); //when the user needs to press the correct button by
	r= random(); //which combination of LEDs lights up
	if (r == 0) { //only k64f blue LED lights up
		LEDBlue_On();
		delay();
		LEDBlue_Toggle();
		while (less_than (current_time, t3)) { //check if user presses correct button within limit
			if ((PTC->PDIR & (1 << 6))==0) {
				int i;
				for (i=0; i<3; i++) {
					delay();
				}
				game(); //if properly pressed, call game again
			}
			else if ((PTA->PDIR & (1 << 4))==0) { //if user presses wrong button, immediately end game
				LEDRed_On();
				while(1);
			}
			else if ((PTA->PDIR & (1 << 1))==0) {
				LEDRed_On();
				while(1);
			}
			else if ((PTB->PDIR & (1 << 23)) == 0) {
				LEDRed_On();
				while(1);
			}
		}
		LEDRed_On(); //if limit exceeded, end game
		while(1);
	}
	else if (r == 1){ //only k64f green on
		LEDGreen_On();
		delay();
		LED_Off();
		while (less_than (current_time, t3)) {
			if ((PTA->PDIR & (1 << 4))==0) {
				int i;
				for (i=0; i<3; i++) {
					delay();
				}
				game();
			}
			else if ((PTC->PDIR & (1 << 6))==0) {
				LEDRed_On();
				while(1);
			}
			else if ((PTA->PDIR & (1 << 1))==0) {
				LEDRed_On();
				while(1);
			}
			else if ((PTB->PDIR & (1 << 23)) == 0) {
				LEDRed_On();
				while(1);
			}
		}
		LEDRed_On();
		while(1);
	}
	else if(r == 2) {
		//only green on mcr20a
		cr20a_green_on();
		delay();
		LED_Off();
		while (less_than (current_time, t3)) {
			if ((PTA->PDIR & (1 << 1))==0) {
				int i;
				for (i=0; i<3; i++) {
					delay();
				}
				game();
			}
			else if ((PTC->PDIR & (1 << 6))==0) {
				LEDRed_On();
				while(1);
			}
			else if ((PTA->PDIR & (1 << 4))==0) {
				LEDRed_On();
			while(1);
			}
			else if ((PTB->PDIR & (1 << 23)) == 0) {
				LEDRed_On();
				while(1);
			}
		} 
		LEDRed_On();
		while(1);
	}
	else if (r==3) {
		//only blue on shield on
		cr20a_blue_on();
		delay();
		LED_Off();
		while (less_than (current_time, t3)) {
			if ((PTB->PDIR & (1 << 23))==0) {
				int i;
				for (i=0; i<5; i++) {
					delay();
				}
				game();
			}
			else if ((PTC->PDIR & (1 << 6))==0) {
				LEDRed_On();
				while(1);
			}
			else if ((PTA->PDIR & (1 << 1))==0) {
				LEDRed_On();
				while(1);
			}
			else if ((PTA->PDIR & (1 << 4)) == 0) {
				LEDRed_On();
			while(1);
			}
			if ((PTA->PDIR & (1 << 1))==0 | (PTC->PDIR & (1 << 6))==0) {
				LEDRed_On();
			while(1);
			}
		}
		LEDRed_On();
		while(1);
	}				
	else if (r==4) { //shield green, k64f blue on
		cr20a_green_on();
		LEDBlue_On();
		delay();
		LED_Off();
		while (less_than (current_time, t3)) {
			if ((PTA->PDIR & (1 << 1))==0) { //check if shield green pressed first
				while (less_than (current_time, t3)) {
					if ((PTC->PDIR & (1 << 6))==0) { //then check if board blue pressed afterwards
						int i;
						for (i=0; i<3; i++){
							delay();
						}
						game();
					}
				}
			}
			if ((PTC->PDIR & (1 << 6))==0) { //check if board blue pressed first
				while (less_than (current_time, t3)) {
					if ((PTA->PDIR & (1 << 1))==0) { //check if shield green pressed afterwards
						int i;
						for (i=0; i<3; i++) {
							delay();
						}
						game();
					}
				}
			}
			if (((PTA->PDIR & (1 << 4))==0) | ((PTB->PDIR & (1 << 23))==0)) { //wrong button press, immediate loss
				LEDRed_On();
				while(1);
			}
		}
		LEDRed_On(); //wait too long, loss
		while(1);
	}
	else if (r==5) {
		cr20a_green_on();
		LEDGreen_On();
		delay();
		LED_Off();
		while (less_than (current_time, t3)) {
			if ((PTA->PDIR & (1 << 1))==0) {
				while (less_than (current_time, t3)) {
					if ((PTA->PDIR & (1 << 4))==0) {
						int i;
						for (i=0; i<3; i++){
							delay();
						}
						game();
					}
				}
			}
			if ((PTA->PDIR & (1 << 4))==0) {
				while (less_than (current_time, t3)) {
					if ((PTA->PDIR & (1 << 1))==0) {
						int i;
						for (i=0; i<3; i++) {
							delay();
						}
						game();
					}
				}
			}
			if (((PTC->PDIR & (1 << 6))==0) | ((PTB->PDIR & (1 << 23))==0)) {
				LEDRed_On();
				while(1);
			}
		}
		LEDRed_On();
		while(1);
	}
	else if (r==6) {
		cr20a_blue_on();
		LEDBlue_On();
		delay();
		LED_Off();
		while (less_than (current_time, t3)) {
			if ((PTB->PDIR & (1 << 23))==0) {
				while (less_than (current_time, t3)) {
					if ((PTC->PDIR & (1 << 6))==0) {
						int i;
						for (i=0; i<3; i++){
							delay();
						}
						game();
					}
				}
			}
			if ((PTC->PDIR & (1 << 6))==0) {
				while (less_than (current_time, t3)) {
					if ((PTB->PDIR & (1 << 23))==0) {
						int i;
						for (i=0; i<3; i++) {
							delay();
						}
						game();
					}
				}
			}
			if ((PTA->PDIR & (1 << 1))==0 | (PTA->PDIR & (1 << 4))==0) {
				LEDRed_On();
				while(1);
			}
		}
		LEDRed_On();
		while(1);
	}
	else if (r==7) {
		cr20a_blue_on();
		LEDGreen_On();
		delay();
		LED_Off();
		while (less_than (current_time, t3)) {
			if ((PTB->PDIR & (1 << 23))==0) {
				while (less_than (current_time, t3)) {
					if ((PTA->PDIR & (1 << 4))==0) {
						int i;
						for (i=0; i<3; i++){
							delay();
						}
						game();
					}
				}
			}
			if ((PTA->PDIR & (1 << 4))==0) {
				while (less_than (current_time, t3)) {
					if ((PTB->PDIR & (1 << 23))==0) {
						int i;
						for (i=0; i<3; i++) {
							delay();
						}
						game();
					}
				}
			}
			if ((PTA->PDIR & (1 << 1))==0 | (PTC->PDIR & (1 << 6))==0) {
				LEDRed_On();
				while(1);
			}
		}
		LEDRed_On();
		while(1);
	}
}



void game_setup (void) {
	SIM->SCGC6 |= SIM_SCGC6_PIT_MASK; // CLOCK PIT
	PIT->MCR &= ~(1 << 1); //Enables clock for PIT timers
	PIT->CHANNEL[0].TCTRL = 001; //Enable timer, no interrupts
	PIT->CHANNEL[1].TCTRL= 3;
	PIT->CHANNEL[1].LDVAL = 0x5136;
	PIT->CHANNEL[0].LDVAL = 0x13FFA10; //Set the load value of the zeroth PIT
	LED_Initialize(); //initialize board LEDs
	cr20_led_initialize(); //initialize shield LEDs
	enable_buttons(); //enable buttons of board and shield
	current_time= (realtime_t*) malloc(sizeof(realtime_t)); //malloc for current_time
	current_time->sec= 0;
	current_time->msec= 0;
	limit.sec= 2; //set initial limit
	limit.msec= 0;
	NVIC_EnableIRQ(PIT1_IRQn); //enable interrupts
}

void PIT1_IRQHandler(void){ //updating clock
	if (current_time->sec == NULL) {
		current_time->sec= 0;
	}
	if (current_time -> msec == NULL) {
		current_time->msec= 0;
	}
	if (current_time->msec== 999) {
		current_time->sec++;
		current_time->msec= 0;
	}	
	else {
		current_time->msec++;
	}
	PIT->CHANNEL[1].LDVAL = 0x5136;
	PIT->CHANNEL[1].TFLG |= (1 << 0);
}

int main(void) {
	game_setup(); //setup game by initializing everything
	cr20a_green_on(); //turn on shield light to make sure shield is properly attached to board
	user_start(); //when user presses SW2 on board
	seed(*start); //seed random number generator
	game(); //play game
	while (1); //loop
}
