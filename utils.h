#ifndef __UTILS_H__
#define __UTILS_H__

void LED_Initialize(void);
void LEDRed_Toggle (void);
void LEDBlue_Toggle (void);
void LEDRed_On (void);
void LEDGreen_On (void);
void LEDBlue_On (void);
void LED_Off (void);
void delay (void);
void cr20_led_initialize(void);
void cr20a_red_on(void);
void cr20a_green_on(void);
void cr20a_green_off(void);
void cr20a_blue_on(void);

#endif

