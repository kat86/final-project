
#include "MK64F12.h"

/*----------------------------------------------------------------------------
  Function that initializes LEDs
 *----------------------------------------------------------------------------*/
 void cr20_led_initialize(void) {
	SIM->SCGC5 |= SIM_SCGC5_PORTC_MASK; //Enable the clock to port C
	//SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK; //Enable the clock to port B
	PORTC->PCR[10]= 1 << 8;
	PORTC->PCR[11] = 1 << 8;
	PORTB->PCR[11]= 1 << 8;
	PTC->PDOR |= (1 << 10);
	PTC->PDDR |= (1 << 10); //red
	PTC->PDOR |= (1 << 11);
	PTC->PDDR |= (1 << 11); //green
	PTB->PDOR |= (1 << 11);
	PTB->PDDR |= (1 << 11); //blue
}
 
void LED_Initialize(void) {
  //SIM->SCGC5    |= (1 <<  10) | (1 <<  13);  /* Enable Clock to Port B & E */ 
  SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK;
	SIM->SCGC5 |= SIM_SCGC5_PORTE_MASK;
	
	PORTB->PCR[22] = (1 <<  8) ;               /* Pin PTB22 is GPIO */
  PORTB->PCR[21] = (1 <<  8);                /* Pin PTB21 is GPIO */
  PORTE->PCR[26] = (1 <<  8);        	/* Pin PTE26  is GPIO */
  
  PTB->PDOR |= (1 << 21 | 1 << 22 );          /* switch Red/Green LED off  */
  PTB->PDDR |= (1 << 21 | 1 << 22 );          /* enable PTB18/19 as Output */

  PTE->PDOR |= 1 << 26;            /* switch Blue LED off  */
  PTE->PDDR |= 1 << 26;            /* enable PTD1 as Output */
}

int red_on = 0;
int blue_on = 0;

/*----------------------------------------------------------------------------
  Function that toggles red LED
 *----------------------------------------------------------------------------*/

void LEDRed_Toggle (void) {
  PIT->CHANNEL[0].TCTRL = 1; //Disable timer to make this function atomic
  if (red_on) {
		PTB->PSOR   = 1 << 22;   /* Red LED Off*/
		red_on = 0;
	} else {
		PTB->PCOR   = 1 << 22;   /* Red LED On*/
		red_on = 1;
	}
	PIT->CHANNEL[0].TCTRL = 3;
}

/*----------------------------------------------------------------------------
  Function that toggles blue LED
 *----------------------------------------------------------------------------*/
void LEDBlue_Toggle (void) {
  PIT->CHANNEL[0].TCTRL = 1;
  if (blue_on) {
		PTB->PSOR   = 1 << 21;   /* Blue LED Off*/
		blue_on = 0;
	} else {
		PTB->PCOR   = 1 << 21;   /* Blue LED On*/
		blue_on = 1;
	}
	PIT->CHANNEL[0].TCTRL = 3;
}

/*----------------------------------------------------------------------------
  Function that turns on Red LED & all the others off
 *----------------------------------------------------------------------------*/
void LEDRed_On (void) {
  PIT->CHANNEL[0].TCTRL = 1;
  PTB->PCOR   = 1 << 22;   /* Red LED On*/
  PTB->PSOR   = 1 << 21;   /* Blue LED Off*/
  PTE->PSOR   = 1 << 26;   /* Green LED Off*/
  red_on      = 1;
  PIT->CHANNEL[0].TCTRL = 3;
  
}

/*----------------------------------------------------------------------------
  Function that turns on Green LED & all the others off
 *----------------------------------------------------------------------------*/
void LEDGreen_On (void) {
  PIT->CHANNEL[0].TCTRL = 1;
  PTB->PSOR   = 1 << 21;   /* Blue LED Off*/
  PTE->PCOR   = 1 << 26;   /* Green LED On*/
  PTB->PSOR   = 1 << 22;   /* Red LED Off*/
  PIT->CHANNEL[0].TCTRL = 3;
}

/*----------------------------------------------------------------------------
  Function that turns on Blue LED & all the others off
 *----------------------------------------------------------------------------*/
void LEDBlue_On (void) {
  PIT->CHANNEL[0].TCTRL = 1;
  PTE->PSOR   = 1 << 26;   /* Green LED Off*/
  PTB->PSOR   = 1 << 22;   /* Red LED Off*/
  PTB->PCOR   = 1 << 21;   /* Blue LED On*/
  blue_on     = 1;
  PIT->CHANNEL[0].TCTRL = 3;
}

/*----------------------------------------------------------------------------
  Function that turns all LEDs off
 *----------------------------------------------------------------------------*/
void LED_Off (void) {
  PIT->CHANNEL[0].TCTRL = 1;
  PTB->PSOR   = 1 << 22;   /* Green LED Off*/
  PTB->PSOR   = 1 << 21;     /* Red LED Off*/
  PTE->PSOR   = 1 << 26;    /* Blue LED Off*/
	PTC->PSOR = 1 << 11;
	PTC->PSOR = 1 << 10;
	PTB->PSOR= 1 << 11;
  PIT->CHANNEL[0].TCTRL = 3;
}

void delay(void){
	int j;
	for(j=0; j<1000000; j++);
}


void cr20a_red_on(void){
	PTC->PCOR  = 1 << 10;
}

void cr20a_green_on(void) {
	PTC->PCOR = 1 << 11;
}

void cr20a_green_off(void) {
	PTC->PSOR = 1 << 11;
}

void cr20a_blue_on(void) {
	PTB->PCOR = 1 << 11;
}

void test_sw2(void) { //test button presses
	PORTA->PCR[1] = (1 << 8);
	while ((PTA->PDIR & (1 << 1))); //while button not pressed, wait- polling
	cr20a_blue_on(); //turn on light
}

void test_sw3(void) {
  PORTB->PCR[23] = (1 << 8);
  while ((PTB->PDIR & (1 << 23)));
  cr20a_green_on();
}